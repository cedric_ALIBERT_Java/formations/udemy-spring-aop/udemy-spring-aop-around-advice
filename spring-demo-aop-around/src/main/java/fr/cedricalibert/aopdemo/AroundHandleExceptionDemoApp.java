package fr.cedricalibert.aopdemo;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.cedricalibert.aopdemo.dao.AccountDAO;
import fr.cedricalibert.aopdemo.dao.MembershipDAO;
import fr.cedricalibert.aopdemo.service.TrafficFortuneService;

public class AroundHandleExceptionDemoApp {

	private static Logger myLogger = 
			Logger.getLogger(AroundHandleExceptionDemoApp.class.getName());
	
	public static void main(String[] args) {
		// read Spring config java class
		AnnotationConfigApplicationContext context = 
				new AnnotationConfigApplicationContext(DemoConfig.class);
		
		
		//get the bean from spring container
		TrafficFortuneService fortuneService = 
				context.getBean("trafficFortuneService", TrafficFortuneService.class);
		
		myLogger.info("Around demo app : main program");
		
		boolean tripWire = true;
		String data = fortuneService.getFortune(tripWire);
		
		myLogger.info("My fortune is : "+data);
		
		myLogger.info("Finish");
		
		//close spring context
		context.close();

	}

}
