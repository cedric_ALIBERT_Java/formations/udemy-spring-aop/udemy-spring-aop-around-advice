package fr.cedricalibert.aopdemo.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import fr.cedricalibert.aopdemo.Account;

@Component
public class AccountDAO {
	
	private String name;
	private String serviceCode;
	
	public void addAccount(Account account, boolean vip) {
		System.out.println(getClass()+"Doing my db work: adding an account");
	}
	
	public boolean doWork() {
		System.out.println(getClass()+"Doing work");
		return true;
	}

	public String getName() {
		System.out.println(getClass()+"getName");
		return name;
	}

	public void setName(String name) {
		System.out.println(getClass()+"setName");
		this.name = name;
	}

	public String getServiceCode() {
		System.out.println(getClass()+"getServiceCode");
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		System.out.println(getClass()+"setServiceCode");
		this.serviceCode = serviceCode;
	}
	
	public List<Account> findAccounts(boolean tripWire){
		
		//for praticing
		if(tripWire) {
			throw new RuntimeException("Practicing simulation");
		}
		
		List<Account> accounts = new ArrayList<Account>();
		
		//create simple accounts and add to list
		Account a1 = new Account("tem1", "1");
		Account a2 = new Account("tem2", "2");
		Account a3 = new Account("tem3", "3");
		
		accounts.add(a1);
		accounts.add(a2);
		accounts.add(a3);
		
		return accounts;
	}
	
	
}
