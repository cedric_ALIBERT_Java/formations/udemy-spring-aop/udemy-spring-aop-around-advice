package fr.cedricalibert.aopdemo;

import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.cedricalibert.aopdemo.dao.AccountDAO;
import fr.cedricalibert.aopdemo.dao.MembershipDAO;

public class AfterFinallyDemoApp {

	public static void main(String[] args) {
		// read Spring config java class
		AnnotationConfigApplicationContext context = 
				new AnnotationConfigApplicationContext(DemoConfig.class);
		
		
		//get the bean from spring container
		AccountDAO accountDAO = context.getBean("accountDAO", AccountDAO.class);
		
		List<Account> accounts = null;
		
		try{
			boolean tripWire = false;
			accounts = accountDAO.findAccounts(tripWire);
		}catch (Exception e) {
			System.out.println("Main program cautgh exeption " + e);
		}
		
		
		System.out.println("Main program : After Finally ");
		System.out.println("------");
		
		System.out.println(accounts);
		
		System.out.println("\n");
		
		//close spring context
		context.close();

	}

}
