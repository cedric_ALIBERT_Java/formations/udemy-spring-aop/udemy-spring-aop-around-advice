package fr.cedricalibert.aopdemo.aspect;

import java.util.List;
import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import fr.cedricalibert.aopdemo.Account;

@Aspect
@Component
@Order(2)
public class MyDemoLoggingAspect {
	
	private Logger myLogger = Logger.getLogger(getClass().getName());
	
	@Around("execution(* fr.cedricalibert.aopdemo.service.*.getFortune(..))")
	public Object aroundGetFortune(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
		
		//display the methods signature
		String methodSign = proceedingJoinPoint.getSignature().toShortString();
						
		myLogger.info("=====> Executing @around on method : "+methodSign);
		
		//start timestamp
		long begin = System.currentTimeMillis();
		
		Object result = null;
				
		try {
			result = proceedingJoinPoint.proceed();
		} catch (Exception e) {
			//log the exeption
			myLogger.warning(e.getMessage());
			
			//result = "Error catch !";
			throw e;
		}
		
		long end = System.currentTimeMillis();
		
		long duration = end-begin;
		
		myLogger.info("\n=====>Duration : "+duration/1000.0+" seconds");
		
		return result;
	}
	
	@After("execution(* fr.cedricalibert.aopdemo.dao.AccountDAO.findAccounts(..))")
	public void afterFinallyFindAccountsAdvice(JoinPoint joinPoint) {
		//display the methods signature
		String methodSign = joinPoint.getSignature().toShortString();
						
		myLogger.info("=====> Executing @after (finally) on method : "+methodSign);
	}
	
	@AfterThrowing(
				pointcut="execution(* fr.cedricalibert.aopdemo.dao.AccountDAO.findAccounts(..))",
				throwing="e"
			)
	public void afterThrowingFindAccountsAdvice(JoinPoint joinPoint, Throwable e) {
		//display the methods signature
		String methodSign = joinPoint.getSignature().toShortString();
						
		myLogger.info("@AfterThrowing : Method : "+methodSign);
		
		myLogger.info("The exeption is : "+e);
	}
	
	@AfterReturning(
			pointcut="execution(* fr.cedricalibert.aopdemo.dao.AccountDAO.findAccounts(..))",
			returning="result"
			)
	public void afterReturningFindAccountsAdvice(JoinPoint joinPoint, List<Account> result) {
		//display the methods signature
		String methodSign = joinPoint.getSignature().toShortString();
				
		myLogger.info("@AfterReturning : Method : "+methodSign);
		
		myLogger.info("Result is : "+ result);
		
		//modify the data post-process
		convertAccountsNamesToUpperCase(result);
		
		//printout the result after modify
		myLogger.info("Result after modify is : "+ result);
		
		
	}
	
	private void convertAccountsNamesToUpperCase(List<Account> result) {
		for(Account account : result) {
			String toUpperName = account.getName().toUpperCase();
			account.setName(toUpperName);
		}
		
	}

	@Before("fr.cedricalibert.aopdemo.aspect.AopExpressions.forDaoPackageExceptSettersAndGetters()")
	public void beforeAddAccountAdvice(JoinPoint joinPoint) {
		myLogger.info("\n======> Executing @Before advice on addAccount");
		
		//display the methods signature
		MethodSignature methodSign = (MethodSignature) joinPoint.getSignature();
		
		myLogger.info("Method : "+methodSign);
		
		//display method argument
		Object[] args = joinPoint.getArgs();
		
		for (Object arg : args) {
			myLogger.info(arg.toString());
			
			if(arg instanceof Account) {
				//downcast and print Account specific stuff
				Account account = (Account) arg;
				myLogger.info("Account Name : "+account.getName());
				myLogger.info("Account Level : "+account.getLevel());
			}
		}
		
	}
	
}
